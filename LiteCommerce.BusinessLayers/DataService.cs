﻿using LiteCommerce.DataLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.BusinessLayers
{
    /// <summary>
    /// các chức năng nghiệp vụ liên quan đến quản lý dữ liệu
    /// </summary>
    public static class DataService
    {
        private static ICountryDAL CountryDB;
        private static ICityDAL CityDB;
        private static ISupplierDAL SupplierDB;
        private static ICategoryDAL CategoryDB;
        private static IShipperDAL ShipperDB;
        private static ICustomerDAL CustomerDB;
        private static IEmployeeDAL EmployeeDB;

        /// <summary>
        /// Khởi tạo tính năng tác nghiệp ( hàm này phải dc gọi nêú muốn sử dụng các tính năng của lớp)
        /// </summary>
        /// <param name="dbType"></param>
        /// <param name="connectionString"></param>
        public static void Init ( DatabaseTypes dbType, string connectionString )
        {
            switch(dbType)
            {
                case DatabaseTypes.SQLServer:
                    CountryDB = new DataLayers.SQLServer.CountryDAL(connectionString);
                    CityDB = new DataLayers.SQLServer.CityDAL(connectionString);
                    SupplierDB = new DataLayers.SQLServer.SupplierDAL(connectionString);
                    CategoryDB = new DataLayers.SQLServer.CategoryDAL(connectionString);
                    ShipperDB = new DataLayers.SQLServer.ShipperDAL(connectionString);
                    CustomerDB = new DataLayers.SQLServer.CustomerDAL(connectionString);
                    EmployeeDB = new DataLayers.SQLServer.EmployeeDAL(connectionString);

                    break;
                default:
                    throw new Exception("Database Type is not supported");
            }
        }

        /// <summary>
        /// Danh sách các quốc gia
        /// </summary>
        /// <returns></returns>
        public static List<Country> ListCountries()
        {
            return CountryDB.List();
        }


        /// <summary>
        /// Danh sách các thành phố
        /// </summary>
        /// <returns></returns>
        public static List<City> ListCities()
        {
            return CityDB.List();
        }


        /// <summary>
        /// danh sách các thành phố thuộc quốc gia
        /// </summary>
        /// <param name="CountryName"></param>
        /// <returns></returns>
        public static List<City> ListCountries(string CountryName)
        {
            return CityDB.List(CountryName);
        }

        /// <summary>
        /// danh sách nhà cung cấp (phân trang, tìm kiếm)
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchValue"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        public static List<Supplier> ListSuppliers(int page, int pageSize, string searchValue, out int rowCount)
        {
            rowCount = SupplierDB.Count(searchValue);
            return SupplierDB.List(page, pageSize, searchValue);
        }

        /// <summary>
        /// Bổ sung nhà cung cấp
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int AddSupplier(Supplier data)
        {
            return SupplierDB.Add(data);
        }


        /// <summary>
        /// Cập nhật nhà cung cấp
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UpdateSupplier(Supplier data)
        {
            return SupplierDB.Update(data);
        }


        /// <summary>
        ///  xóa nhà cung cấp
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public static bool DeleteSupplier(int supplierID)
        {
            return SupplierDB.Delete(supplierID);
        }

        /// <summary>
        /// lấy thông tin nhà cung vấp
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public static Supplier GetSupplier(int supplierID)
        {
            return SupplierDB.Get(supplierID);
        }

        public static List<Category> ListCategories(int page, int pageSize, string searchValue, out int rowCount)
        {
            rowCount = CategoryDB.Count(searchValue);
            return CategoryDB.List(page, pageSize, searchValue);
        }
        public static int AddCategory(Category data)
        {
            return CategoryDB.Add(data);
        }


        public static bool UpdateCategory(Category data)
        {
            return CategoryDB.Update(data);
        }

        public static bool DeleteCategory(int categoryID)
        {
            return CategoryDB.Delete(categoryID);
        }

        public static Category GetCategory(int categoryID)
        {
            return CategoryDB.Get(categoryID);
        }




        public static List<Shipper> ListShippers(int page, int pageSize, string searchValue, out int rowCount)
        {
            rowCount = ShipperDB.Count(searchValue);
            return ShipperDB.List(page, pageSize, searchValue);
        }
        public static int AddShipper(Shipper data)
        {
            return ShipperDB.Add(data);
        }


        public static bool UpdateShipper(Shipper data)
        {
            return ShipperDB.Update(data);
        }

        public static bool DeleteShipper(int shipperID)
        {
            return ShipperDB.Delete(shipperID);
        }

        public static Shipper GetShipper(int shipperID)
        {
            return ShipperDB.Get(shipperID);
        }







        public static List<Customer> ListCustomers(int page, int pageSize, string searchValue, out int rowCount)
        {
            rowCount = CustomerDB.Count(searchValue);
            return CustomerDB.List(page, pageSize, searchValue);
        }
        public static int AddCustomer(Customer data)
        {
            return CustomerDB.Add(data);
        }


        public static bool UpdateCustomer(Customer data)
        {
            return CustomerDB.Update(data);
        }

        public static bool DeleteCustomer(int customerID)
        {
            return CustomerDB.Delete(customerID);
        }

        public static Customer GetCustomer(int customerID)
        {
            return CustomerDB.Get(customerID);
        }




        public static List<Employee> ListEmployees(int page, int pageSize, string searchValue, out int rowCount)
        {
            rowCount = EmployeeDB.Count(searchValue);
            return EmployeeDB.List(page, pageSize, searchValue);
        }
        public static int AddEmployee(Employee data)
        {
            return EmployeeDB.Add(data);
        }


        public static bool UpdateEmployee(Employee data)
        {
            return EmployeeDB.Update(data);
        }

        public static bool DeleteEmployee(int employeeID)
        {
            return EmployeeDB.Delete(employeeID);
        }

        public static Employee GetEmployee(int employeeID)
        {
            return EmployeeDB.Get(employeeID);
        }
    }
}
