﻿using LiteCommerce.DataLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.BusinessLayers
{
    
    /// <summary>
    /// Lớp cung cấp các chức năng tác nghiệp liên quan đến quản lí nhân sự
    /// </summary>
    public static class HRService
    {
        private static IEmployeeDAL EmployeeDB;
        /// <summary>
        /// Hàm khởi tạo tần nghiệp vụ
        /// (Hàm này phải được gọi trước khi sử dụng các chức năng khác trong lớp)
        /// </summary>
        /// <param name="dbTypes">Loại CSDL</param>
        /// <param name="connectionString">Chuỗi tham số kết nối</param>
        public static void Init(DatabaseTypes dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseTypes.SQLServer:
                    EmployeeDB = new LiteCommerce.DataLayers.SQLServer.EmployeeDAL(connectionString);
                    break;
                case DatabaseTypes.FakeDB:
                    break;
                default:
                    throw new Exception("Database Type is not supported");
            }
        }

        /// <summary>
        /// Danh sách nhân viên
        /// </summary>
        /// <returns></returns>
        public static List<Employee> Employee_List()
        {
            return EmployeeDB.List();
        }
    }
}
