﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DataLayers;
using LiteCommerce.DataLayers.SQLServer;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    public class ShipperController : Controller
    {
        // GET: Shipper
        

        public string connectionString = ConfigurationManager.ConnectionStrings["LiteCommerceDB"].ConnectionString;
        public ActionResult Index(int page = 1, string searchValue = "")
        {
            int rowCount = 0;
            int pageSize = 10;
            var listOfShippers = DataService.ListShippers(page, pageSize, searchValue, out rowCount);
            var model = new Models.ShipperPaginationQueryResult()
            {
                Page = page,
                PageSize = pageSize,
                SearchValue = searchValue,
                RowCount = rowCount,
                Data = listOfShippers
            };
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Sữa đổi thông tin nhà vận chuyển";

            IShipperDAL dal = new ShipperDAL(connectionString);
            var data = dal.Get(id);
            return View(data);
        }
        public ActionResult Add(Shipper shipperDB)
        {
            ViewBag.Title = "Thêm nhà vận chuyển";


            /*var data = new ShipperDAL(connectionString);
            data.Add(shipperDB);*/
            return View("Edit");
        }
        public ActionResult Delete(string id)
        {
            return View();
        }
        public ActionResult Save()
        {


            return RedirectToAction("Index");
        }
    }
}