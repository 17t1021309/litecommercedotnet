﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LiteCommerce.DataLayers;
using LiteCommerce.DataLayers.SQLServer;
using LiteCommerce.DomainModels;

namespace LiteCommerce.Admin.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string connectionString = ConfigurationManager.ConnectionStrings["LiteCommerceDB"].ConnectionString;
        public ActionResult Index()
        {
            
            /*ICountryDAL dal = new CountryDAL(connectionString);
            var data = dal.List();*/

            /* List
             * 
             * ICityDAL dal = new CityDAL(connectionString);
            var data = dal.List("");*/

            //Get
       
         ISupplierDAL dal = new SupplierDAL(connectionString);
            var data = dal.Get(1);

            /*
             *  add
                        ISupplierDAL dal = new SupplierDAL(connectionString);
                        Supplier s = new Supplier()
                        {
                            SupplierName = "Le Viet",
                            ContactName = "viet IT",
                            Country = "Vietnam",
                            Address = "quantri",
                            City = "dong ha",
                            PostalCode =" 52000",
                            Phone = "0123456780"
                        };
                        int supplierID = dal.Add(s);
                        var data = dal.Get(supplierID);*/

            //update
            /* ISupplierDAL dal = new SupplierDAL(connectionString);
             Supplier s = new Supplier()
             {
                 SupplierID = 30,
                 SupplierName = "Le Xuan Viet",
                 ContactName = "viet IT",
                 Country = "Vietnam",
                 Address = "quantri",
                 City = "dong ha",
                 PostalCode = " 52000",
                 Phone = "0123456780"
             };
             var data = dal.Update(s);*/

           /* Delete
            ISupplierDAL dal = new SupplierDAL(connectionString);
            var data = dal.Delete(30);*/


            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Pagination(int page, int pageSize, string searchValue)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["LiteCommerceDB"].ConnectionString;
            /*ISupplierDAL dal = new SupplierDAL(connectionString);
            var data = dal.List(page, pageSize, searchValue);*/

            /*
            ICustomerDAL dal = new CustomerDAL(connectionString);
            var data = dal.List(page, pageSize, searchValue);*/


            /*IShipperDAL dal = new ShipperDAL(connectionString);
            var data = dal.List(page, pageSize, searchValue);*/

            ICategoryDAL dal = new CategoryDAL(connectionString);
            var data = dal.List(page, pageSize, searchValue);

            return Json(data, JsonRequestBehavior.AllowGet);


            //Test: Url: https://localhost:44353/Test/Pagination?page=2&pageSize=10&searchValue=
        }
    }
}