﻿using LiteCommerce.BusinessLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index(int page =1, string searchValue ="")
        {
            int rowCount = 0;
            int pageSize = 10;
            var listOfEmployees = DataService.ListEmployees(page, pageSize, searchValue, out rowCount);
            var model = new Models.EmployeePaginationQueryResult()
            {
                Page = page,
                PageSize = pageSize,
                SearchValue = searchValue,
                RowCount= rowCount,
                Data = listOfEmployees
            };

            return View(model);
        }

        public ActionResult Edit(string id)
        {
            ViewBag.Title = "Sữa đổi thông tin nhân viên";
            return View();
        }
        public ActionResult Add(string id)
        {
            ViewBag.Title = "Thêm nhân viên";
            return View("Edit");
        }
        public ActionResult Delete(string id)
        {
            return View();
        }
        public ActionResult Save()
        {
            return RedirectToAction("Index");
        }
    }
}