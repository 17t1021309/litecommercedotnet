﻿using LiteCommerce.BusinessLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    public class SupplierController : Controller
    {
        // GET: Supplier
        public ActionResult Index(int page = 1, string searchValue ="")
        {
            /*int rowCount = 0;
            int pageSize = 10;
            var listOfSuppliers = DataService.ListSuppliers(page, pageSize, searchValue, out rowCount);
            int pageCount = rowCount / 10;
            if(rowCount % pageSize > 0)
            {
                pageCount += 1;
            }

            ViewBag.Page = page;
            ViewBag.RowCount = rowCount;
            ViewBag.PageCount = pageCount;
            ViewBag.SearchValue = searchValue;
            return View(listOfSuppliers);*/

            int rowCount = 0;
            int pageSize = 10;
            var listOfSuppliers = DataService.ListSuppliers(page, pageSize, searchValue, out rowCount);
            /*Models.SupplierPaginationQueryResult model = new Models.SupplierPaginationQueryResult()*/
            var model = new Models.SupplierPaginationQueryResult()
            {
                Page = page,
                PageSize = pageSize,
                SearchValue = searchValue,
                RowCount = rowCount,
                Data = listOfSuppliers
            };
            return View(model);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(string id)
        {
            ViewBag.Title = "Sữa đổi thông tin nhà cung cấp";
            return View();
        }
        public ActionResult Add(string id)
        {
            ViewBag.Title = "Thêm nhà cung cấp";
            return View("Edit");
        }
        public ActionResult Delete(string id)
        {
            return View();
        }
        public ActionResult Save()
        {
            return RedirectToAction("Index");
        }
    }
}