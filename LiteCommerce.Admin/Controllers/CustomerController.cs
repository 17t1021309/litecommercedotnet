﻿using LiteCommerce.BusinessLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index(int page = 1, string searchValue = "")
        {
            int rowCount = 0;
            int pageSize = 10;
            var listOfCustomers = DataService.ListCustomers(page, pageSize, searchValue, out rowCount);
            var model = new Models.CustomerPaginationQueryResult()
            {
                Page = page,
                PageSize = pageSize,
                SearchValue = searchValue,
                RowCount = rowCount,
                Data = listOfCustomers
            };
            return View(model) ;
        }
        public ActionResult Add(string id)
        {
            ViewBag.Title = "Thêm khách hàng";
            return View("Edit");
        }
        public ActionResult Edit(string id)
        {
            ViewBag.Title = "Sữa thông tin khách hàng";
            return View();
        }
        public ActionResult Save()
        {
            return RedirectToAction("Index");
        }
        public ActionResult Delete(string id)
        {
            return View();
        }
    }
}