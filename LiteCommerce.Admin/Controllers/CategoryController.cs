﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DataLayers.SQLServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index(int page =1, string searchValue  ="")
        {
            int rowCount = 0;
            int pageSize = 10;
            var listOfCategorys = DataService.ListCategories(page, pageSize, searchValue, out rowCount);
            var model = new Models.CategoryPaginationQueryResult()
            {
                Page = page,
                PageSize = pageSize,
                SearchValue = searchValue,
                RowCount = rowCount,
                Data = listOfCategorys
            };
            return View(model);
        }
        public ActionResult Add(string id)
        {
            ViewBag.Title = "Thêm loại hàng";
            return View("Edit");
        }
        public ActionResult Edit(string id)
        {
            ViewBag.Title = "Sữa loại hàng";
            return View();
        }
        public ActionResult Save()
        {
            return RedirectToAction("Index");
        }
        public ActionResult Delete(string id)
        {
            return View();
        }
    }
}