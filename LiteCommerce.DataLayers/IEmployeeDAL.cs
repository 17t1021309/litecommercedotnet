﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    /// <summary>
    /// Định nghĩa các phép xử lý dữ liệu liên quan đến nhân viên
    /// </summary>
    ///
    public interface IEmployeeDAL
    {
        List<Employee> List();
        List<Employee> List(int page, int pageSize, string searchValue);
        int Add(Employee data);
        int Count(string searchValue);
        Employee Get(int employeeID);
        bool Update(Employee data);
        bool Delete(int employeeID);
    }
}
