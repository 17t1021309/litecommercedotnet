﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    public interface ICategoryDAL
    {
        int Add(Category data);



        List<Category> List(int page, int pageSize, string searchValue);



        int Count(string searchValue);



        Category Get(int categoryID);



        bool Update(Category data);



        bool Delete(int categoryID);
    }
}
