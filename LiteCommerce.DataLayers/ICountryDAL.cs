﻿using LiteCommerce.DomainModels;
using System.Collections.Generic;

namespace LiteCommerce.DataLayers
{
    /// <summary>
    /// khai báo các phép xử lý dữ liệu liên quan đến quốc gia
    /// </summary>
    public interface ICountryDAL
    {
        /// <summary>
        /// lấy danh sách tất cả các quốc gia
        /// </summary>
        /// <returns></returns>
        List<Country> List();
    }
}
