﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    public interface IShipperDAL
    {

        int Add(Shipper data);
        List<Shipper> List(int page, int pageSize, string searchValue);



        int Count(string searchValue);


        Shipper Get(int shipperID);


        bool Update(Shipper data);


        bool Delete(int shipperID);
    }
}
