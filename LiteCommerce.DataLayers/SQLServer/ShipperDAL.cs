﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers.SQLServer
{
    public class ShipperDAL : _BaseDAL, IShipperDAL
    {
        public ShipperDAL(string connectionString) : base(connectionString)
        {

        }

        public int Add(Shipper data)
        {
            int shipperID = 0;

            using ( SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = cn.CreateCommand();
                cmd.CommandText = @"INSERT INTO Shippers(
                                     ShipperName, Phone)
                                    VALUES(
                                    @ShipperName, @Phone );
                                    SELECT @@IDENTITY;";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ShipperName", data.ShipperName);
                cmd.Parameters.AddWithValue("@Phone", data.Phone);

                shipperID = Convert.ToInt32(cmd.ExecuteScalar());
                cn.Close();
            }

            return shipperID;
        }

        public int Count(string searchValue)
        {
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";
            int result = 0;

            using(SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = cn.CreateCommand();
                cmd.CommandText = @"SELECT COUNT(*) FROM Shippers 
                                WHERE (@searchValue ='')
                                OR (
                                ShipperName LIKE @searchValue 
                                OR Phone LIKE @searchValue)";

                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                result = Convert.ToInt32(cmd.ExecuteScalar());
                cn.Close();
            }


            return result;
        }

        public bool Delete(int shipperID)
        {
            bool result = false;
            
            using (SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = cn.CreateCommand();
                cmd.CommandText = @"DELETE FROM Shippers WHERE ShipperID = @ShipperID
                                    AND NOT EXISTS(
                                    SELECT * FROM Orders where ShipperID = Orders.ShipperID)";

                cmd.Parameters.AddWithValue("@ShipperID", shipperID);
                result = cmd.ExecuteNonQuery() > 0;
                cn.Close();
            }


            return result;
        }

        public Shipper Get(int shipperID)
        {
            Shipper data = null;

            using (SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = cn.CreateCommand();
                cmd.CommandText = @"SELECT * FROM Shippers WHERE ShipperID = @shipperID";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ShipperID", shipperID);

                using(SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if(dbReader.Read())
                    {
                        data = new Shipper()
                        {
                            ShipperID = Convert.ToInt32(dbReader["ShipperID"]),
                            ShipperName = Convert.ToString(dbReader["ShipperName"]),
                            Phone = Convert.ToString(dbReader["Phone"])
                        };
                    }
                }
            }


            return data;
        }

        public List<Shipper> List(int page, int pageSize, string searchValue)
        {
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            List<Shipper> data = new List<Shipper>();
            using (SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT * FROM ( SELECT *, ROW_NUMBER() over(ORDER BY ShipperName) AS RowNumber 
                                    From Shippers WHERE(@searchValue = '')
                                    OR (
                                        ShipperName LIKE @searchValue 
                                        OR ShipperID LIKE @searchValue
                                        OR Phone LIKE @searchValue )) AS s
                                    WHERE s.RowNumber BETWEEN (@page - 1)*@pageSize + 1 AND @page*@pageSize";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {

                        data.Add(new Shipper()
                        {
                            ShipperID = Convert.ToInt32(dbReader["ShipperID"]),
                            ShipperName = Convert.ToString(dbReader["ShipperName"]),
                            Phone = Convert.ToString(dbReader["Phone"])
                           
                        });
                    }
                }


                cn.Close();
            }

            return data;
        }

        public bool Update(Shipper data)
        {
            bool result = false;

            using (SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = cn.CreateCommand();
                cmd.CommandText = @"UPDATE Shippers 
                                    SET ShipperName = @ShipperName,
                                    Phone = @Phone                               
                                    where ShipperID = @shipperID;";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ShipperID", data.ShipperID);
                cmd.Parameters.AddWithValue("@ShipperName", data.ShipperName);
                cmd.Parameters.AddWithValue("@Phone", data.Phone);

                result = cmd.ExecuteNonQuery() > 0;
                cn.Close();
            }

            return result;
        }
    }
}
