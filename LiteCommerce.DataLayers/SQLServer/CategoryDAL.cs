﻿
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers.SQLServer
{
    public class CategoryDAL : _BaseDAL, ICategoryDAL
    {
        public CategoryDAL(string connectionString) : base(connectionString)
        {

        }

        public int Add(Category data)
        {
            int categoryID = 0;
            using (SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = cn.CreateCommand();
                cmd.CommandText = @"INSERT INTO Suppliers(
                            CategoryName, Description, ParentCategoryId)
                            VALUES(
                            @CategoryName, @Description, @ParentCategoryId );
                            SELECT @@IDENTITY;";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@CategoryName", data.CategoryName);
                cmd.Parameters.AddWithValue("@Description", data.Description);
                cmd.Parameters.AddWithValue("@ParentCategoryId", data.ParentCategoryId);
                categoryID = Convert.ToInt32(cmd.ExecuteScalar());
                cn.Close();
            }

            return categoryID;
        }

        public int Count(string searchValue)
        {
            if (searchValue != null)
            {
                searchValue = "%" + searchValue + "%";
            }


            int result = 0;

            using (SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = cn.CreateCommand();
                cmd.CommandText = @"SELECT COUNT(*) FROM Categories 
                                WHERE (@searchValue ='')
                                OR (
                                    CategoryName LIKE @searchValue 
                                    OR Description LIKE @searchValue)";

                cmd.Parameters.AddWithValue("@searchValue", searchValue);

                result = Convert.ToInt32(cmd.ExecuteScalar());
                cn.Close();
            }
            return result;
        }

        public bool Delete(int categoryID)
        {
            bool result = false;

            using( SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = cn.CreateCommand();
                cmd.CommandText = @"DELETE FROM Categories WHERE CategoryID = @CategoryID
                                     AND NOT EXISTS(SELECT * FROM Products where 
                                    CategoryID = Categories.CategoryID)";

                cmd.Parameters.AddWithValue("@CategoryID", categoryID);

                result = cmd.ExecuteNonQuery() > 0;
                cn.Close();
            }

            return result;
        }

        public Category Get(int categoryID)
        {
            Category data = null;

            using (SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = cn.CreateCommand();
                cmd.CommandText = @" SELECT * FROM Categories WHERE CategoryID = @CategoryID";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@CategoryID", categoryID);

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if(dbReader.Read())
                    {
                        data = new Category()
                        {
                            CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"]),
                            ParentCategoryId = Convert.ToInt32(dbReader["ParentCategoryId"])
                        };
                    }
                }
            }


            return data;
        }

        public List<Category> List(int page, int pageSize, string searchValue)
        {
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            List<Category> data = new List<Category>();
            using (SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT * FROM ( SELECT *, ROW_NUMBER() over(ORDER BY CategoryName) AS RowNumber 
                                    From Categories WHERE(@searchValue = '')
                                    OR (
                                        CategoryID LIKE @searchValue 
                                        OR CategoryName LIKE @searchValue 
                                        OR Description LIKE @searchValue
                                        OR ParentCategoryId LIKE @searchValue)) AS s
                                    WHERE s.RowNumber BETWEEN (@page - 1)*@pageSize + 1 AND @page*@pageSize";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {

                        data.Add(new Category()
                        {
                            CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"]),
                            Description = Convert.ToString(dbReader["Description"]),
                            /*ParentCategoryId = Convert.ToInt32(dbReader["ParentCategoryId"])*/
                        });
                    }
                }


                cn.Close();
            }

            return data;
        }

        public bool Update(Category data)
        {
            bool result = false;

            using(SqlConnection cn = GetConnection())
            {
                SqlCommand cmd = cn.CreateCommand();
                cmd.CommandText = @" UPDATE Categories 
                                        SET CategoryName = @CategoryName,
                                        Description = @Description,
                                        ParentCategoryId = @ParentCategoryId                                       
                                        where CategoryID = @CategoryID;";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@CategoryID", data.CategoryID);
                cmd.Parameters.AddWithValue("@CategoryName", data.CategoryName);
                cmd.Parameters.AddWithValue("@ParentCategoryId", data.ParentCategoryId);


                result = cmd.ExecuteNonQuery() > 0;

                cn.Close();
            }

            return result;
        }
    }
}
